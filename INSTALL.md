# Installation

Pour installer Meet-N-Roll sur votre site web il vous faudra tout d'abord compiler le projet avec Maven, pour celà lancez simplement la commande 'mvn clean install' à la racine du projet en configurant correctement la balise `<directory>VOTRE_REPERTOIRE</directory>` du pom.xml pour qu'il pointe vers votre dossier webapp/Meet-N-Roll/ de votre tomcat.

## Configuration du Tomcat

Verifiez que le context.xml a la racine du projet ressemble bien à ceci :

`<Context>
    <Resource name="jdbc/sqlite"
    type="javax.sql.DataSource"
    driverClassName="org.sqlite.JDBC"
    url="jdbc:sqlite:../webapps/Meet-N-Roll/data/Database.db"
    />
</Context>`

Vous serez peut être amené à modifier la valeur de l'URL si vous souhaitez déplacer la base de donnée.


Configurez aussi le conf/web.xml de votre Tomcat en ajoutant cette portion de code après la balise `<web-app>` :

  `<resource-ref>
    <res-ref-name>jdbc/sqlite</res-ref-name>
    <res-type>javax.sql.DataSource</res-type>
  </resource-ref>`