var _parameterMap = new Map();

function acknowledgeParameter()
{
	var queryString = window.location.search;
	queryString = queryString.substring(1);
	var parameterArray = queryString.split("&");

	for (var i = 0; i < parameterArray.length; i++)
	{
		if (parameterArray[i].indexOf("=") !== -1)
		{
			_parameterMap.set(parameterArray[i].split("=")[0], parameterArray[i].split("=")[1]);
		}
	}
}

function getParameter(param)
{
	if (typeof _parameterMap.get(param)  === "undefined")
	{
		console.log(param + " is undefined");
		return null;
	}
	return _parameterMap.get(param);
}
