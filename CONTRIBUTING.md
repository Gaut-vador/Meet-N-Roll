# Contribution

Vous pouvez contribuer au projet Meet-N-Roll en soummettant de nouvelles `merge-request`. Pour celà, créez votre propre branche à partir de la branche `develop` pour y commiter vos modifications. Une fois toutes vos modifications prêtes, soummettez une `merge request`. Si vos modifications son pertinantes, elles seront mergé avec la branche `develop` et ajoutées au projet, sinon elles seront oubliées dans les méandres sombre et pleine de microbes de l'oublie.

/!\ Attention, seules les modifications fonctionnelles ou les corrections de typos seront acceptée. Tout ce qui concerne la propretée du code ou son indentation sera automatiquement rejetée.
Les changements sur la mise en formes des pages web pourront être discutés en amont.

Pour plus d'informations, contactez-moi directement à cette adresse : gauthier.pirlet@tutanota.com 