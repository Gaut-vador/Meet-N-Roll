package types.criterias;

import java.util.List;

public class CriteriaStringType {
	private String equals;
	private List<String> inList;
	
	public String getEquals()
	{
		return equals;
	}
	
	public void setEquals(String pEquals)
	{
		equals = pEquals;
	}
	
	public List<String> getInList()
	{
		return inList;
	}
	
	public void setInList(List<String> pInList)
	{
		inList = pInList;
	}
}
