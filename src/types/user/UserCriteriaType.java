package types.user;

import types.criterias.CriteriaStringType;

public class UserCriteriaType {
	
	private CriteriaStringType name;
	
	public UserCriteriaType()
	{
		name = new CriteriaStringType();
	}
	
	public void setName(CriteriaStringType pName)
	{
		name = pName;
	}
	
	public CriteriaStringType getName()
	{
		return name;
	}
}
