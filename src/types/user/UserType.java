package types.user;

import java.util.Date;

import types.GameList;
import types.GameType;

/**
 * Objet regroupant les informations d'un utilisateur
 * 
 * @author gaut-vador
 *
 */
public class UserType
{
	private Integer _id;
	private String _login;
	private String _mail;
	private Date _birthday;
	private String _town;
	private String _region;
	private GameType _gameType;
	private GameList _gameList;

	public UserType()
	{
		super();
	}

	public Date getBirthday()
	{
		return _birthday;
	}

	public Integer getId()
	{
		return _id;
	}

	public void setId(Integer pId)
	{
		this._id = pId;
	}

	public String getLogin()
	{
		return _login;
	}

	public void setLogin(String pLogin)
	{
		this._login = pLogin;
	}

	public String getMail()
	{
		return _mail;
	}

	public void setMail(String pMail)
	{
		this._mail = pMail;
	}

	public String getTown()
	{
		return _town;
	}

	public void setTown(String pTown)
	{
		this._town = pTown;
	}

	public String getRegion()
	{
		return _region;
	}

	public void setRegion(String pRegion)
	{
		this._region = pRegion;
	}

	public GameType getGameType()
	{
		return _gameType;
	}

	public void setGameType(GameType pGameType)
	{
		this._gameType = pGameType;
	}

	public GameList getGameList()
	{
		return _gameList;
	}

	public void setGameList(GameList pGameList)
	{
		this._gameList = pGameList;
	}

	public void setBirthday(Date pBirthday)
	{
		_birthday = pBirthday;
	}
}