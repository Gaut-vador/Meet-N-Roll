package manager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import utils.Logger;

public class DatabaseConnectionManager
{

	/** Instance unique non préinitialisée */
	private static DatabaseConnectionManager INSTANCE = null;
	private DataSource dataSource = null;
	private Connection connection = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;

	/** Constructeur privé */
	private DatabaseConnectionManager()
	{
		try
		{
			Class.forName("org.sqlite.JDBC");
			Context context = new InitialContext();
			dataSource = (DataSource) context.lookup("java:comp/env/jdbc/sqlite");
			connect();
		} catch (ClassNotFoundException e)
		{
			Logger.writeException(e);
		} catch (NamingException e)
		{
			Logger.writeException(e);
		}
	}

	/** Point d'accès pour l'instance unique du singleton */
	public static DatabaseConnectionManager getInstance()
	{
		if (INSTANCE == null)
		{
			synchronized (DatabaseConnectionManager.class)
			{
				if (INSTANCE == null)
				{
					INSTANCE = new DatabaseConnectionManager();
				}
			}
		}
		return INSTANCE;
	}

	public Connection connect()
	{
		try
		{
			connection = dataSource.getConnection();
		} catch (SQLException e)
		{
			Logger.writeException(e);
		}
		return connection;
	}
	
	public Statement getStatement()
	{
		try
		{
			return connection.createStatement();
		} catch (SQLException e)
		{
			Logger.writeException(e);
		}
		return null;
	}

	public PreparedStatement executeQuerry(String sql, Object[] args)
	{
		try
		{
			preparedStatement = connection.prepareStatement(sql);
			for (int i = 1; i <= args.length; i++)
			{
				if (args[i - 1]instanceof String)
				{
					preparedStatement.setString(i, (String) args[i - 1]);
				}
				else if (args[i - 1] instanceof Integer)
				{
					preparedStatement.setInt(i, (int) args[i-1]);
				}
			}

			preparedStatement.executeUpdate();
		} catch (SQLException e)
		{
			Logger.writeException(e);
		}
		return preparedStatement;
	}

	public void closeAll()
	{
		try
		{
			if (statement != null)
			{
				statement.close();
			}
			if (preparedStatement != null)
			{
				preparedStatement.close();
			}
			if (connection != null)
			{
				connection.close();
			}
		} catch (SQLException e)
		{
			Logger.writeException(e);
		}
	}
}
