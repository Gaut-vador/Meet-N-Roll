package manager;

import java.sql.ResultSet;
import java.sql.SQLException;

import types.enums.UserExistanceEnum;
import types.user.UserType;

public class UserManager
{

	public static UserExistanceEnum isUserExist(String pLogin, String pMail)
	{
		DatabaseConnectionManager dbConnectionManager = null;
		ResultSet resultSet = null;
		String querryLogin = "SELECT * FROM USERS WHERE LOGIN=" + pLogin + ";";
		String querryMail = "SELECT * FROM USERS WHERE MAIL=" + pMail + ";";
		try
		{
			dbConnectionManager = DatabaseConnectionManager.getInstance();
			dbConnectionManager.connect();

			resultSet = dbConnectionManager.getStatement().executeQuery(querryLogin);
			while (resultSet.next())
			{
				return UserExistanceEnum.LOGIN_EXIST;
			}

			resultSet = dbConnectionManager.getStatement().executeQuery(querryMail);
			while (resultSet.next())
			{
				return UserExistanceEnum.MAIL_EXIST;
			}
		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();

		} finally
		{
			if (dbConnectionManager != null)
			{
				dbConnectionManager.closeAll();
			}
		}
		return UserExistanceEnum.DO_NOT_EXIST;
	}

	/**
	 * Ajoute un utilisateur et retourne l'id de celui-ci
	 * 
	 * @param pUser
	 * @return ID de l'utilisateur
	 */
	public static int addUser(UserType pUser)
	{

		/*
		 * Check si l'utilisateur n'existe pas deja -> l'utilisateur (mail et ou pseudo)
		 * existe -> message erreur -> l'utilisateur n'existe pas, on l'enregistre dans
		 * la table USERS et dans USERS_LOGIN
		 */
		DatabaseConnectionManager dbConnectionManager = null;
		String queryHeader = "INSERT INTO USERS (LOGIN, BIRTHDAY, MAIL, TOWN, REGION) VALUES (?,?,?,?,?);";
		try
		{
			dbConnectionManager = DatabaseConnectionManager.getInstance();
			dbConnectionManager.connect();

			Object[] argsHeader = { pUser.getLogin(), pUser.getBirthday(), pUser.getMail(), pUser.getTown(),
					pUser.getRegion() };
			dbConnectionManager.executeQuerry(queryHeader, argsHeader);

			// recuperation de l'id de l'utilisateur créé

			String getIDQuery = "SELECT ID FROM USERS WHERE LOGIN='" + pUser.getLogin() + "';";
			ResultSet resultId = dbConnectionManager.getStatement().executeQuery(getIDQuery);

			while (resultId.next())
			{
				return resultId.getInt("ID");
			}

		} catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally
		{
			if (dbConnectionManager != null)
			{
				dbConnectionManager.closeAll();
			}
		}
		return -1;

	}

}
