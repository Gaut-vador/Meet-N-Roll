package manager.web.servlet;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import manager.UserManager;
import types.enums.UserExistanceEnum;
import types.user.UserType;
import utils.LoginUtils;

@WebServlet("/singUp")
public class SingUpManager extends HttpServlet
{

	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		if (!request.getParameter("login").equals("") && !request.getParameter("password").equals("")
				&& !request.getParameter("mail").equals("")
				&& request.getParameter("password").equals(request.getParameter("passwordConfirmation")))
		{

			UserExistanceEnum userExistance = UserManager.isUserExist(request.getParameter("login"),
					request.getParameter("mail"));
			switch (userExistance)
			{
			case DO_NOT_EXIST:
				UserType newUser = new UserType();
				newUser.setLogin(request.getParameter("login"));
				newUser.setMail(request.getParameter("mail"));
				newUser.setBirthday(Date.valueOf(request.getParameter("birthdate")));
				int userId = UserManager.addUser(newUser);
				newUser.setId(userId);
				LoginUtils.addLogin(newUser, request.getParameter("password"));
				response.sendRedirect("Succes");
				break;
			case LOGIN_EXIST:
				response.sendRedirect("../login.html");
				break;
			case MAIL_EXIST:
				response.sendRedirect("../login.html");
				break;
			default:
				break;

			}

			response.sendRedirect("../login.html");

		} else
		{
			response.sendRedirect("../new.html");
		}
	}
}
