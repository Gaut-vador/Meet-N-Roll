package manager.web.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import types.user.UserType;
import utils.LoginUtils;

@WebServlet("login")
public class LoginManager extends HttpServlet
{

	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		if (request != null)
		{
			HttpSession session = request.getSession();

			if (request.getParameter("delog") != null && request.getParameter("delog").equals("true") && session != null)
			{
				session.invalidate();
			} else
			{
				UserType user = LoginUtils.logIntoWebsite(request.getParameter("login"), request.getParameter("password"));

				if (user != null)
				{
					session.setAttribute("user", user);
					response.sendRedirect("/Meet-N-Roll/servlet/Menu");
				} else
				{
					// TODO ajouter message dans HTML "Login ou mdp incorrect"
					response.sendRedirect("../login.html");
				}

			}
		}
	}
}
