package utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;

public class Logger
{
	private static final String LOG_PATH = /* ".." + File.separator + */"Logs_MeetNRoll" + File.separator;

	public Logger()
	{

	}

	public static void writeException(Exception pException)
	{

		String logFilePath = "ExceptionDetails.log";
		FileWriter fileWriter = null;
		BufferedWriter output = null;
		try
		{
			File path = new File(LOG_PATH);
			if (!path.exists())
			{
				path.mkdirs();
			}
			File fichier = new File(LOG_PATH + logFilePath);
			if (!fichier.exists())
			{
				fichier.createNewFile();
			}
			fileWriter = new FileWriter(fichier, true);
			output = new BufferedWriter(fileWriter);
			Date dateOfError = new Date();
			DateFormat shortDateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
			output.write(shortDateFormat.format(dateOfError) + " : " + pException.getMessage() + "\n");
			for (StackTraceElement stackTraceElement : pException.getStackTrace())
			{
				output.write("\t" + stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + "(line: "
						+ stackTraceElement.getLineNumber() + ")\n");
			}
			output.flush();
		} catch (IOException e)
		{
			e.printStackTrace();
		} finally
		{
			if (fileWriter != null)
			{
				try
				{
					fileWriter.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
			if (output != null)
			{
				try
				{
					output.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
	}
}
