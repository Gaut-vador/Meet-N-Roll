package utils;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import manager.DatabaseConnectionManager;
import manager.UserManager;
import types.user.UserType;

public class LoginUtils
{
	public static UserType logIntoWebsite(String pUsername, String pPassword) throws IOException
	{
		UserType result = null;
		if (pUsername != null && pPassword != null)
		{
			result = checkIdentification(pUsername, pPassword);
			if (result != null)
			{
				UserManager databaseUser = new UserManager();
				// result = databaseUser.getUser(result);
			}
		}
		return result;
	}

	public static void addLogin(UserType pUser, String pPassword)
	{
		String querry = "INSERT INTO USERS_LOGIN (ID, LOGIN, PASSWORD) VALUES (?,?,?)";
		Object[] args = { pUser.getId(), pUser.getLogin(), EncryptUtils.encryptPassword(pPassword) };

		DatabaseConnectionManager databaseConnection = DatabaseConnectionManager.getInstance();
		databaseConnection.executeQuerry(querry, args);
	}

	private static UserType checkIdentification(String pLogin, String pPassword) throws IOException
	{
		UserType result = null;
		String encryptedPassword = EncryptUtils.encryptPassword(pPassword);
		String querry = "SELECT * FROM USERS_LOGIN WHERE LOGIN = '" + pLogin + "' AND PASSWORD = '" + encryptedPassword
				+ "';";
		DatabaseConnectionManager databaseConnection = DatabaseConnectionManager.getInstance();
		try
		{
			ResultSet resultSet = databaseConnection.getStatement().executeQuery(querry);

			if (resultSet.next())
			{
				result = new UserType();
				result.setId(resultSet.getInt(0));
				result.setLogin(resultSet.getString(1));
			}
		} catch (SQLException e)
		{
			Logger.writeException(e);
		}
		return result;
	}

}
