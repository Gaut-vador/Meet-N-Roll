package utils;

import org.apache.commons.codec.digest.DigestUtils;

public class EncryptUtils
{
	public static String encryptPassword(String pPassword)
	{
		String encryptedPassword = DigestUtils.sha256Hex(pPassword);
		return encryptedPassword;
	}
}
