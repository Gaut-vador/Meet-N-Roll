# Meet-N-Roll

## Introduction
Projet de site web de recontre entre rôliste.
Le but de ce projet est de permettre a des joueur-se-s d'en rencontrer d'autres, basé sur leurs affinités en terme de jeux.
Les principales fontionnalités sont :
<ul> 
	<li>Creer un profile avec un pseudo, une adresse mail, et des types de jeux favoris/haït</li>
	<li>Rencontrer via un mode de "match" d'autres joueur-se-s et/ou GM en fonction des affinités en terme de jeux définies dans le profile</li>
	<li>Rechercher des utilisateur-trice-s</li>
	<li>Tchatter avec les autres utilisateurs</li>
	<li>Organiser des rencontres avec plusieurs personnes via un calendrier intégré</li>
	<li>Se tenir informé des dernières nouveautes rôliste/GN via la page principale</li>
</ul>

## Installation

Pour les informations concernant l'installation de ce site, repportez vous au fichier INSTALL.md 

## Contribution

Pour savoir comment contribuer au projet, reportez vous au fichier CONTRIBUTING.md

